﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using UnfuddleAPIHelper;

namespace UnfuddleAPIHelperTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("testing UnfuddleAPIHelper...");
            Console.WriteLine("");

            Settings settings = new Settings();
            settings.ApiVersion = "1";
            settings.UseSsl = true;
            settings.Subdomain = ConfigurationManager.AppSettings["Unfuddle.Subdomain"];
            settings.Username = ConfigurationManager.AppSettings["Unfuddle.Username"];
            settings.Password = ConfigurationManager.AppSettings["Unfuddle.Password"];


            Ticket t = new Ticket(settings);
            t.ProjectId = 3;
            t.GetByNumber(1);
            //t.CreatedAt = DateTime.Now;
            t.Description = "blah blah balh x";
            t.DescriptionFormat = Format.MARKDOWN;
            //t.DescriptionFormatted = false;
            t.Priority = Priority.THREE;
            //t.Resolution = Resolution.WORKS_FOR_ME;
            t.Summary = "updated ticket via api";
            t.Status = Status.NEW;
            bool result = t.Update();
            Console.WriteLine("-------------: tickte Update? " + result + ", " + t.Id);

            //List<Ticket> list = t.GetList();
            //foreach (Ticket item in list)
            //{
            //    Console.WriteLine(item.ToXml());
            //    Console.WriteLine("-------------");
            //}


            //Project p = new Project(settings);
            //p.GetByName("project");
            //Console.WriteLine(p.ToXml());


            //Backup o = new Backup();
            //o.CreatedAt = DateTime.Now;
            //o.Parts = 222;
            //o.ProjectID = 2;


            Console.WriteLine("");
            Console.WriteLine("finished");
            Console.WriteLine("");
        }
    }
}
