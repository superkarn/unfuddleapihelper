﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// NOTE: Not fully tested
namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/tickets
    /// http://unfuddle.com/docs/api/data_models#subscription
    /// 
    ///<subscription>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <parent-id type="integer"> </parent-id>
    ///  <parent-type> [Ticket] </parent-type>
    ///  <person-id type="integer"> </person-id>
    ///  <updated-at type="datetime"> </updated-at>
    ///</subscription>
    /// </summary>
    [XmlName("subscription")]
    [XmlListName("subscriptions")]
    public class Subscription : BaseModel
    {
        public int ProjectId;
        public int TicketId { get { return this.ParentId; } set { this.ParentId = value; } }


        #region XML Fields
        [IncludeInXml]
        [ReadOnly]
        [XmlName("parent-id")]
        public int ParentId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("parent-type")]
        public ParentType ParentType;

        [IncludeInXml]
        [XmlName("person-id")]
        public int PersonId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion



        public Subscription(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/subscriptions",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/subscriptions/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/subscriptions/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/subscriptions",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/subscriptions/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<Subscription> GetList()
        {
            return this.GetListHelper(this.GetUriGetList(), null).ConvertAll<Subscription>(x => (Subscription)x);
        }
        #endregion




        #region Non-public methods
        protected override void CheckRequiredData()
        {
            base.CheckRequiredData();


            if (this.ProjectId == 0) throw new Exception("ProjectId is empty.");
            if (this.TicketId == 0) throw new Exception("TicketId is empty.");
        }
        #endregion
    }
}
