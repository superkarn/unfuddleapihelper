﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    public static class Utility
    {

        /// <summary>
        /// Converts a string into the specified type.
        /// </summary>
        public static object ConvertFromString(Type t, string s)
        {
            object result = null;



            if (t == typeof(AssigneeOnResolve))
            {
                try { result = Enum.Parse(typeof(AssigneeOnResolve), s, true); }
                catch { result = AssigneeOnResolve.NULL; }
            }

            if (t == typeof(bool))
            {
                try { result = bool.Parse(s); }
                catch { result = false; }
            }

            if (t == typeof(DateTime))
            {
                try { result = DateTime.Parse(s); }
                catch { result = DateTime.MinValue; }
            }

            if (t == typeof(float) || t == typeof(System.Single))
            {
                try { result = float.Parse(s); }
                catch { result = 0; }
            }

            if (t == typeof(int))
            {
                try { result = int.Parse(s); }
                catch { result = 0; }
            }

            if (t == typeof(MarkupFormat))
            {
                try { result = Enum.Parse(typeof(MarkupFormat), s, true); }
                catch { result = MarkupFormat.NULL; }
            }

            if (t == typeof(ParentType))
            {
                try { result = Enum.Parse(typeof(ParentType), s, true); }
                catch { result = ParentType.NULL; }
            }

            if (t == typeof(Priority))
            {
                try { result = Enum.Parse(typeof(Priority), s, true); }
                catch { result = Priority.NULL; }
            }

            if (t == typeof(Relationship))
            {
                try { result = Enum.Parse(typeof(Relationship), s, true); }
                catch { result = Relationship.NULL; }
            }

            if (t == typeof(Resolution))
            {
                try { result = Enum.Parse(typeof(Resolution), s, true); }
                catch { result = Resolution.NULL; }
            }

            if (t == typeof(Status))
            {
                try { result = Enum.Parse(typeof(Status), s, true); }
                catch { result = Status.NULL; }
            }

            if (t == typeof(string))
            {
                result = s;
            }

            if (t == typeof(Theme))
            {
                try { result = Enum.Parse(typeof(Theme), s, true); }
                catch { result = Theme.NULL; }
            }

            if (t == typeof(TicketDisposition))
            {
                try { result = Enum.Parse(typeof(TicketDisposition), s, true); }
                catch { result = TicketDisposition.NULL; }
            }




            //Console.WriteLine("        [" + t + "]: [" + s + "] -> [" + result + "]");
            return result;
        }

        /// <summary>
        /// Converts a type into a string
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string ConvertToString(AssigneeOnResolve o)
        {
            if (o == AssigneeOnResolve.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(bool o)
        {
            return (o + "").ToLower();
        }
        public static string ConvertToString(DateTime o)
        {
            if(o == DateTime.MinValue) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(double o)
        {
            return (o + "").ToLower();
        }
        public static string ConvertToString(float o)
        {
            return (o + "").ToLower();
        }
        public static string ConvertToString(int o)
        {
            return (o + "").ToLower();
        }
        public static string ConvertToString(MarkupFormat o)
        {
            if (o == MarkupFormat.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(object o)
        {
            return (o + "").ToLower();
        }
        public static string ConvertToString(ParentType o)
        {
            if (o == ParentType.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(Priority o)
        {
            if (o == Priority.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(Relationship o)
        {
            if (o == Relationship.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(Resolution o)
        {
            if (o == Resolution.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(Status o)
        {
            if (o == Status.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(string o)
        {
            return o;
        }
        public static string ConvertToString(Theme o)
        {
            if (o == Theme.NULL) return "";
            else return (o + "").ToLower();
        }
        public static string ConvertToString(TicketDisposition o)
        {
            if (o == TicketDisposition.NULL) return "";
            else return (o + "").ToLower();
        }

        
    }
}
