﻿using System;

namespace UnfuddleAPIHelper
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class IncludeInXml : System.Attribute { }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class IncludeInGetRequest : System.Attribute { }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class IncludeInPostRequest : System.Attribute { }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ReadOnly : System.Attribute { }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class RequiredInXml : System.Attribute { }

    [AttributeUsage(AttributeTargets.Class)]
    public class XmlListName : System.Attribute
    {
        string Value;

        public XmlListName(string value)
        {
            this.Value = value;
        }

        public string GetValue()
        {
            return this.Value;
        }
    }

    [AttributeUsage(
        AttributeTargets.Class |
        AttributeTargets.Field | 
        AttributeTargets.Property)]
    public class XmlName : System.Attribute
    {
        string Value;

        public XmlName(string value)
        {
            this.Value = value;
        }

        public string GetValue()
        {
            return this.Value;
        }
    }
}
