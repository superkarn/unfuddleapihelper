﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace UnfuddleAPIHelper
{
    public class Settings
    {
        public string ApiVersion = "1";
        public string Subdomain = "";
        public bool UseSsl = false;
        public string Username = "";
        public string Password = "";
    }
}
