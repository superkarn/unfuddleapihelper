﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#severity
    ///
    ///<severity>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <name> </name>
    ///  <project-id type="integer"> </project-id>
    ///  <updated-at type="datetime"> </updated-at>
    ///</severity>
    /// </summary>
    [XmlName("severity")]
    public class Severity : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [XmlName("name")]
        public string Name;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("project-id")]
        public int ProjectID;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion


        public Severity(Settings settings)
            : base(settings)
        { }
    }
}
