﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    public enum AssigneeOnResolve
    {
        NULL,
        REPORTER,
        NONE,
        NOCHANGE
    }

    public enum MarkupFormat
    {
        NULL,
        MARKDOWN, 
        PLAIN,
        TEXTILE, 
    }

    public enum NotificationFrequency
    {
        NULL,
        IMMEDIATE,
        //30MINS,  __TODO__ find a way to deal with "30mins" option
        HOURLY,
        DAILYAM,
        DAILYPM,
        NEVER,
    }

    public enum NotificationScope
    {
        NULL,
        ALL,
        INVOLVED,  // __TODO__ not available for all scopes...
        NONE,
    }

    public enum ParentType
    {
        NULL,
        ACCOUNT,
        PROJECT,
        TICKET,
    }

    public enum PermissionMessage
    {
        Manage,
        None,
        Read,
        ReadCreate,
    }
    public enum PermissionMilestone
    {
        Manage,
        None,
        Read,
    }

    public enum PermissionNotebook
    {
        Manage,
        None,
        Read,
    }

    public enum PermissionPeople
    {
        Invite,
        Manage,
        None,
        Read,
    }

    public enum PermissionSource
    {
        Commit,
        None,
        Read,
    }

    public enum PermissionTicket
    {
        create,
        Manage,
        None,
        Read,
        ReadCreate,
    }

    public enum Priority
    {
        NULL,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
    }

    public enum Relationship
    {
        NULL,
        DUPLICATE,
        PARENT,
        RELATED,
    }

    public enum Resolution
    {
        NULL,
        DUPLICATE, 
        FIXED, 
        INVALID,
        POSTPONED, 
        WILL_NOT_FIX, 
        WORKS_FOR_ME, 
    }

    public enum SearchFilter
    {
        NULL,
        Changesets, 
        Comments, 
        Messages, 
        Milestones, 
        Notebooks, 
        Tickets,
    }

    public enum SearchGroupBy
    {
        NULL,
        Assignee,
        Component,
        DueOn,
        Milestone,
        Priority,
        Reporter,
        Resolution,
        Severity,
        Status,
        Version,
    }

    public enum SearchSortBy
    {
        NULL,
        Assignee,
        Association,
        ClosedOn,
        Comments,
        Component,
        CreatedAt,
        DueOn,
        HoursActual,
        HoursEstimateInitial,
        HoursEstimateCurrent,
        LastCommentAt,
        Milestone,
        Number,
        PercentageComplete,
        Priority,
        Reporter,
        Resolution,
        Severity,
        Status,
        Summary,
        UpdatedAt,
        Version,
    }

    public enum SearchSortDirection
    {
        Ascending,
        Descending,
    }

    public enum Status
    {
        NULL,
        ACCEPTED, 
        CLOSED,
        NEW,  
        REASSIGNED, 
        REOPENED, 
        RESOLVED, 
        UNACCEPTED,
    }

    public enum Theme
    {
        NULL,
        BLUE,
        GREEN,
        GREY,
        ORANGE,
        PURPLE,
        RED,
        TEAL
    }

    public enum TicketDisposition
    {
        NULL,
        LIST,
        TEXT
    }
}
