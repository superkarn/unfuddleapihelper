﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#time_entry
    /// 
    ///<person>
    ///  <!-- only available for the /api/v1/Person/current endpoint -->
    ///  <access-key> </access-key>
    ///  <account-id type="integer"> </account-id>
    ///  <created-at type="datetime"> </created-at>
    ///  <email> </email>
    ///  <first-name> </first-name>
    ///  <id type="integer"> </id>
    ///  <!--
    ///  Within the Person model, setting 'is-administrator' to true will
    ///  make the person an account administrator.
    ///  -->
    ///  <is-administrator type="boolean"> [true, false] </is-administrator>
    ///  <is-removed type="boolean"> [true, false] </is-removed>
    ///  <last-name> </last-name>
    ///  <last-signed-in type="datetime"> </last-signed-in>
    ///  <last-signed-in-formatted> <!-- only available if formatted=true --> </last-signed-in-formatted>
    ///  <notification-frequency> [immediate, 30mins, hourly, dailyam, dailypm, never] </notification-frequency>
    ///  <notification-ignore-self type="boolean"> [true, false] </notification-ignore-self>
    ///  <notification-last-sent type="datetime"> </notification-last-sent>
    ///  <notification-scope-messages> [all, none] </notification-scope-messages>
    ///  <notification-scope-milestones> [all, none] </notification-scope-milestones>
    ///  <notification-scope-source> [all, none] </notification-scope-source>
    ///  <notification-scope-tickets> [all, involved, none] </notification-scope-tickets>
    ///  <show-help-messages type="boolean"> [true, false] </show-help-messages>
    ///  <text-markup> [markdown, textile, plain] </text-markup>
    ///  <time-zone> <!-- i.e. "Pacific Time (US & Canada)" --> </time-zone>
    ///  <updated-at type="datetime"> </updated-at>
    ///  <!-- The username it is not available for removed Person -->
    ///  <username> </username>
    ///</person>
    /// </summary>
    [XmlName("person")]
    [XmlListName("people")]
    public class Person : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [ReadOnly]
        [XmlName("access-key")]
        public string AccessKey;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("account-id")]
        public int AccessId;

        [IncludeInXml]
        [XmlName("email")]
        public string Email;

        [IncludeInXml]
        [XmlName("first-name")]
        public string FirstName;

        [IncludeInXml]
        [XmlName("is-administrator")]
        public bool IsAdministrator;

        [IncludeInXml]
        [XmlName("is-removed")]
        public bool IsRemoved;

        [IncludeInXml]
        [XmlName("last-name")]
        public string LastName;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("last-signed-in")]
        public DateTime LastSignedIn;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("last-signed-in-formatted")]
        public bool LastSignedInFormatted;

        [IncludeInXml]
        [XmlName("notification-frequency")]
        public NotificationFrequency NotificationFrequency;

        [IncludeInXml]
        [XmlName("notification-ignore-self")]
        public bool NotificationIgnoreSelf;

        [IncludeInXml]
        [XmlName("notification-last-sent")]
        public DateTime NotificationLastSent;

        [IncludeInXml]
        [XmlName("notification-scope-messages")]
        public NotificationScope NotificationScopeMessages;

        [IncludeInXml]
        [XmlName("notification-scope-milestones")]
        public NotificationScope NotificationScopeMilestones;

        [IncludeInXml]
        [XmlName("notification-scope-source")]
        public NotificationScope NotificationScopeSource;

        [IncludeInXml]
        [XmlName("notification-scope-tickets")]
        public NotificationScope NotificationScopeTickets;

        [IncludeInXml]
        [XmlName("show-help-messages")]
        public bool ShowHlpMessages;

        [IncludeInXml]
        [XmlName("text-markup")]
        public MarkupFormat TextMarkup;

        [IncludeInXml]
        [XmlName("time-zone")]
        public string TimeZone;  // __TODO__ an example given: Pacific Time (US & Canada)

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;

        [IncludeInXml]
        [XmlName("username")]
        public string Username;
        #endregion


        public Person(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<Person> GetList(PersonOptions options = null)
        {
            return this.GetListHelper(this.GetUriGetList(), options).ConvertAll<Person>(x => (Person)x);
        }
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<Person> GetListByProjectId(int id)
        {
            this.CheckRequiredData();
            if (id == 0) throw new Exception("Project Id is empty.");

            string uri =
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/people",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    id
                );

            return this.GetListHelper(uri, null).ConvertAll<Person>(x => (Person)x);
        }

        /// <summary>
        /// Return a list of project this person is involved in.
        /// </summary>
        /// <returns></returns>
        public List<Involvement> GetInvolvements()
        {
            Involvement obj = new Involvement(this.Settings);
            return obj.GetListByPersonId(this.Id);
        }
        #endregion




        #region Non-public methods
        protected override void CheckRequiredData()
        {
            base.CheckRequiredData();
        }
        #endregion
    }


    public class PersonOptions : BaseOptions
    {
        [IncludeInGetRequest]
        [XmlName("removed")]
        public bool Removed;
    }
}
