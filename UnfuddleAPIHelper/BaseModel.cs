﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;

namespace UnfuddleAPIHelper
{
    public class BaseModel
    {
        protected Settings Settings;


        #region XML Fields (available to all types)
        [IncludeInXml]
        [ReadOnly]
        [XmlName("id")]
        public int Id;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("created-at")]
        public DateTime CreatedAt;
        #endregion


        
        public BaseModel(Settings settings)
        {
            this.Settings = settings;
        }



        #region Generic CRUD methods
        /// <summary>
        /// Create a new object
        /// </summary>
        public int Create()
        {
            this.CheckRequiredData();

            byte[] byteArray = Encoding.UTF8.GetBytes(this.ToXml());

            // send request
            WebRequest request = WebRequest.Create(this.GetUriCreate());
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = byteArray.Length;
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }


            // get response
            WebResponse response = request.GetResponse();
            string newItemUri = response.Headers["Location"];

            // new Id is the number after the last /
            string newIDStr = newItemUri.Substring(newItemUri.LastIndexOf("/") + 1);

            // sometimes the location has file format extension, so get rid of it
            if (newIDStr.IndexOf(".") > -1) newIDStr = newIDStr.Substring(0, newIDStr.IndexOf(".") + 1);

            response.Close();

            return int.Parse(newIDStr);
        }

        /// <summary>
        /// Delete an object
        /// 
        /// For now, this will return true if the deletion was successful.
        /// It will throw http errors
        ///     - 404 if the object is not found (maybe already deleted)
        ///     
        /// To delete, we just need to call DELETE to the URI.  No need
        /// to send any data.
        /// </summary>
        public bool Delete()
        {
            this.CheckRequiredData();
            if (this.Id == 0) throw new Exception("Id is empty.");

            // send request
            WebRequest request = WebRequest.Create(this.GetUriDelete());
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "DELETE";
            request.ContentType = "application/xml";


            // get response
            WebResponse response = request.GetResponse();
            response.Close();


            return true;
        }

        /// <summary>
        /// Given the id, call Unfuddle and load the data into the current object.
        /// </summary>
        public void GetById(int id)
        {
            this.CheckRequiredData();
            if (id == 0) throw new Exception("Id is empty.");

            // send request
            WebRequest request = WebRequest.Create(this.GetUriGetById(id));
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "GET";
            request.ContentType = "application/xml";


            // get response
            WebResponse response = request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    string responseFromServer = reader.ReadToEnd();
                    this.LoadItemFromXml(responseFromServer, true);
                }
            }
            response.Close();
        }

        /// <summary>
        /// Get a list of all projects
        /// All of which inherits the same Settings
        /// This method needs to be overridden in the subclasses
        /// to cast the object list
        /// </summary>
        public List<BaseModel> GetListHelper(string uri, BaseOptions options = null)
        {
            this.CheckRequiredData();

            // add options
            if (options != null)
            {
                uri += options.ConvertToGetParameters();
            }
            //Console.WriteLine("uri: " + uri);

            // send request
            WebRequest request = WebRequest.Create(uri);
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "GET";
            request.ContentType = "application/xml";


            // get response
            List<BaseModel> result = null;
            WebResponse response = request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    string responseFromServer = reader.ReadToEnd();
                    result = this.LoadListFromXml(responseFromServer);
                }
            }
            response.Close();

            return result;
        }

        /// <summary>
        /// Update an object
        /// 
        /// Right now the update works (the object is updated), 
        /// but for some reason the server still returns 500 error.
        /// </summary>
        public bool Update()
        {
            this.CheckRequiredData();
            if (this.Id == 0) throw new Exception("Id is empty.");

            byte[] byteArray = Encoding.UTF8.GetBytes(this.ToXml());

            // send request
            WebRequest request = WebRequest.Create(this.GetUriUpdate());
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "PUT";
            request.ContentType = "application/xml";
            request.ContentLength = byteArray.Length;
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }


            // get response
            WebResponse response = request.GetResponse();
            response.Close();

            return true;
        }
        #endregion


        #region Get Uri methods
        protected virtual string GetUriCreate()
        {
            throw new NotImplementedException();
        }
        protected virtual string GetUriDelete()
        {
            throw new NotImplementedException();
        }
        protected virtual string GetUriGetById(int id)
        {
            throw new NotImplementedException();
        }
        protected virtual string GetUriGetList()
        {
            throw new NotImplementedException();
        }
        protected virtual string GetUriUpdate()
        {
            throw new NotImplementedException();
        }
        #endregion



        /// <summary>
        /// Check to make sure we have all the required data before calling the API.
        /// e.g. APIVersion, Subdomain, Username, Password
        /// </summary>
        protected virtual void CheckRequiredData()
        {
            if (this.Settings.ApiVersion == "") throw new Exception("ApiVersion is empty.");
            if (this.Settings.Subdomain == "") throw new Exception("Subdomain is empty.");
            if (this.Settings.Username == "") throw new Exception("Username is empty.");
            if (this.Settings.Password == "") throw new Exception("Password is empty.");
        }

        /// <summary>
        /// Converts the object into Xml
        /// </summary>
        /// <param name="includeReadOnlyFields">If true, include the ReadOnly fields in the Xml.</param>
        /// <returns></returns>
        public string ToXml(bool includeReadOnlyFields = false)
        {
            string xml = "";
            using (MemoryStream msReq = new MemoryStream())
            {
                string xmlElementName = "";
                {
                    Attribute[] attributes = Attribute.GetCustomAttributes(this.GetType());
                    foreach (Attribute attribute in attributes)
                    {
                        if (attribute is XmlName)
                        {
                            xmlElementName = ((XmlName)attribute).GetValue();
                            break;
                        }
                    }
                }


                XmlTextWriter writer = new XmlTextWriter(msReq, Encoding.Default);
                writer.WriteStartElement(xmlElementName);


                // loop through all the fields
                FieldInfo[] fields = this.GetType().GetFields();
                foreach (var field in fields)
                {
                    bool includeInXml = false;
                    bool readOnly = false;
                    bool requiredInXml = false;
                    string xmlFieldName = "";
                    string xmlFieldValue = "";

                    // find the ones that are required in the xml
                    Attribute[] attributes = Attribute.GetCustomAttributes(field);
                    foreach (Attribute attribute in attributes)
                    {
                        if (attribute is IncludeInXml)
                        {
                            includeInXml = true;
                        }

                        if (attribute is ReadOnly)
                        {
                            readOnly = true;
                        }

                        if (attribute is RequiredInXml)
                        {
                            requiredInXml = true;
                        }

                        if (attribute is XmlName)
                        {
                            string value = ((XmlName)attribute).GetValue();
                            xmlFieldName = value != "" ? value : xmlFieldName;
                        }
                    }

                    // if this field should not be included in the Xml, go to the next field
                    if (!includeInXml) continue;

                    // set the value based on type
                    xmlFieldValue = Utility.ConvertToString(field.GetValue(this));


                    // __TODO__ create a new Exception class for this
                    // if a required field is empty, throw an exception
                    if (requiredInXml && xmlFieldValue == "")
                    {
                        throw new Exception(field.Name + " is required.");
                    }


                    // ignore readOnly fields if the opion doesn't require it
                    if (!includeReadOnlyFields && readOnly) continue;

                    // ignore empty fields
                    if (xmlFieldValue == "") continue;


                    // write the element to the Xml
                    writer.WriteElementString(xmlFieldName, xmlFieldValue);
                }


                writer.WriteEndElement();
                writer.Close();

                xml = Encoding.Default.GetString(msReq.ToArray());

                writer.Close();
            }

            return xml;
        }

        /// <summary>
        /// Load an array of objects from XML
        /// The root element name should match the object and should contain attribute type="array"
        /// 
        /// If the root is valid, xmlIsValid is true.
        /// </summary>
        internal List<BaseModel> LoadListFromXml(string xml)
        {
            List<BaseModel> result = new List<BaseModel>();

            MemberInfo info = this.GetType();
            XmlListName xmlListName = Attribute.GetCustomAttribute(info, typeof(XmlListName)) as XmlListName;
            XmlName xmlName = Attribute.GetCustomAttribute(info, typeof(XmlName)) as XmlName;

            bool xmlIsValid = false;

            XmlTextReader reader = new XmlTextReader(new StringReader(xml));
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    string name = reader.Name;

                    // We could check for root's attribute[type] == "array", but don't need to?
                    //if (name == attribute.GetValue() && reader.GetAttribute("type") == "array")
                    if (name == xmlListName.GetValue())
                    {
                        xmlIsValid = true;
                    }
                    else
                    {
                        if (!xmlIsValid)
                        {
                            reader.Close();

                            if (name == "nil-classes") return result;

                            throw new Exception("Invalid XML.");
                        }

                        // look for the child elements that matches the object's XmlName
                        // then create a new object from the values
                        // and add to result
                        if (name == xmlName.GetValue())
                        {
                            result.Add(this.LoadItemFromXml(reader.ReadOuterXml()));
                        }
                    }
                }
            }
            reader.Close();

            return result;
        }

        /// <summary>
        /// Load the object from XML
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="loadToSelf">
        ///     true: load the data from xml to self
        ///     false: load the data from xml to a new object and return it
        /// </param>
        /// <returns></returns>
        internal BaseModel LoadItemFromXml(string xml, bool loadToSelf = false)
        {
            BaseModel result = null;

            XmlTextReader reader = new XmlTextReader(new StringReader(xml));
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    string name = reader.Name;
                    reader.Read();

                    FieldInfo[] fields = this.GetType().GetFields();
                    foreach (FieldInfo field in fields)
                    {
                        XmlName attribute = Attribute.GetCustomAttribute(field, typeof(XmlName)) as XmlName;
                        if (attribute != null && attribute.GetValue() == name)
                        {
                            if (loadToSelf)
                            {
                                // set the value for the respective field
                                field.SetValue(this, Utility.ConvertFromString(field.FieldType, reader.Value));
                            }
                            else
                            {
                                // if result is null, and we found a valid data, 
                                // instantiate the result object to the same type as the current object
                                if (result == null)
                                {
                                    result = (BaseModel)Activator.CreateInstance(this.GetType(), this.Settings);
                                }

                                // set the value for the respective field
                                field.SetValue(result, Utility.ConvertFromString(field.FieldType, reader.Value));
                            }
                        }
                    }
                }
            }
            reader.Close();

            return result;
        }
    }


    /// <summary>
    /// This class represents the options that you can pass in with the request.
    /// </summary>
    public class BaseOptions
    {
        /// <summary>
        /// This function converts the options into GET parameter format
        /// e.g. ?&key1=val1&key2=val2&key3=val3
        /// 
        /// It uses the XmlName attribute as the key, 
        /// but replaces dashes "-" with underscores "_".
        /// </summary>
        internal string ConvertToGetParameters()
        {
            string result = "?";

            FieldInfo[] fields = this.GetType().GetFields();
            foreach (FieldInfo field in fields)
            {
                IncludeInGetRequest includeInGetRequest = Attribute.GetCustomAttribute(field, typeof(IncludeInGetRequest)) as IncludeInGetRequest;

                if (includeInGetRequest != null)
                {
                    string key = (Attribute.GetCustomAttribute(field, typeof(XmlName)) as XmlName).GetValue().Replace("-", "_");
                    string value = Utility.ConvertToString(field.GetValue(this));
                    if (key != null && value != "")
                    {
                        result += "&" + key + "=" + value;
                    }
                }
            }

            return result;
        }
    }
}
