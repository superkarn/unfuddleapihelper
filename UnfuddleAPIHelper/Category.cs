﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#category
    ///
    ///<category>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <name> </name>
    ///  <project-id type="integer"> </project-id>
    ///  <updated-at type="datetime"> </updated-at>
    ///</category>
    /// </summary>
    [XmlName("category")]
    public class Category : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [XmlName("name")]
        public string Name;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("project-id")]
        public int ProjectId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion



        public Category(Settings settings)
            : base(settings)
        { }
    }
}
