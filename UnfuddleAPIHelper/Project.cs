﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/projects
    /// http://unfuddle.com/docs/api/data_models#project
    /// 
    ///<project>
    ///    <account-id type="integer"> </account-id>
    ///    <archived type="boolean"> [true, false] </archived>
    ///    <assignee-on-resolve> [reporter, none, nochange] </assignee-on-resolve>
    ///    <backup-frequency type="integer">0</backup-frequency>
    ///    <close-ticket-simultaneously-default type="boolean"> [true, false] </close-ticket-simultaneously-default>
    ///    <created-at type="datetime"> </created-at>
    ///    <default-ticket-report-id type="integer"> </default-ticket-report-id>
    ///    <description> </description>
    ///    <disk-usage type="integer"> <!-- in Kilobytes --> </disk-usage>
    ///    <enable-time-tracking type="boolean"> [true, false] </enable-time-tracking>
    ///    <id type="integer"> </id>
    ///    <s3-access-key-id></s3-access-key-id>
    ///    <s3-backup-enabled type="boolean">false</s3-backup-enabled>
    ///    <s3-bucket-name></s3-bucket-name>
    ///    <short-name> </short-name>
    ///    <theme> [blue, green, grey, orange, purple, red, teal] </theme>
    ///    <ticket-field1-active type="boolean"> [true, false] </ticket-field1-active>
    ///    <ticket-field1-title> </ticket-field1-title>
    ///    <ticket-field1-disposition> [list, text] </ticket-field1-disposition>
    ///    <ticket-field2-active type="boolean"> [true, false] </ticket-field2-active>
    ///    <ticket-field2-title> </ticket-field2-title>
    ///    <ticket-field2-disposition> [list, text] </ticket-field2-disposition>
    ///    <ticket-field3-active type="boolean"> [true, false] </ticket-field3-active>
    ///    <ticket-field3-title> </ticket-field3-title>
    ///    <ticket-field3-disposition> [list, text] </ticket-field3-disposition>
    ///    <title> </title>
    ///    <updated-at type="datetime"> </updated-at>
    ///</project>
    /// </summary>
    [XmlName("project")]
    [XmlListName("projects")]
    public class Project : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [ReadOnly]
        [XmlName("account-id")]
        public int AccountID;

        [IncludeInXml]
        [XmlName("archived")]
        public bool Archived;

        [IncludeInXml]
        [XmlName("assignee-on-resolve")]
        public AssigneeOnResolve AssigneeOnResolve;

        [IncludeInXml]
        [XmlName("backup-frequency")]
        public int BackupFrequency;

        [IncludeInXml]
        [XmlName("close-ticket-simultaneously-default")]
        public bool CloseTicketSimultaneouslyDefault;

        [IncludeInXml]
        [XmlName("default-ticket-report-id")]
        public int DefaultTicketReportID;

        [IncludeInXml]
        [XmlName("description")]
        public string Description;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("disk-usage")]
        public int DiskUsage;

        [IncludeInXml]
        [XmlName("enable-time-tracking")]
        public bool EnableTimeTracking;

        [IncludeInXml]
        [XmlName("s3-access-key-id")]
        public string S3AccessKeyId;

        [IncludeInXml]
        [XmlName("s3-backup-enabled")]
        public bool S3BackupEnabled;

        [IncludeInXml]
        [XmlName("s3-bucket-name")]
        public string S3BucketName;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("short-name")]
        public string ShortName;

        [IncludeInXml]
        [XmlName("theme")]
        public Theme Theme;

        [IncludeInXml]
        [XmlName("ticket-field1-active")]
        public bool TicketField1Active;

        [IncludeInXml]
        [XmlName("ticket-field1-title")]
        public string TicketField1Title;

        [IncludeInXml]
        [XmlName("ticket-field1-disposition")]
        public TicketDisposition TicketField1Disposition;

        [IncludeInXml]
        [XmlName("ticket-field2-active")]
        public bool TicketField2Active;

        [IncludeInXml]
        [XmlName("ticket-field2-title")]
        public string TicketField2Title;

        [IncludeInXml]
        [XmlName("ticket-field2-disposition")]
        public TicketDisposition TicketField2Disposition;

        [IncludeInXml]
        [XmlName("ticket-field3-active")]
        public bool TicketField3Active;

        [IncludeInXml]
        [XmlName("ticket-field3-title")]
        public string TicketField3Title;

        [IncludeInXml]
        [XmlName("ticket-field3-disposition")]
        public TicketDisposition TicketField3Disposition;

        [IncludeInXml]
        [XmlName("title")]
        public string Title;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion



        public Project(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        public void GetByShortName(string name)
        {
            this.CheckRequiredData();
            if (name == "") throw new Exception("Name  is required.");

            string uri =
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/by_short_name/{3}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    name
                );


            // send request
            WebRequest request = WebRequest.Create(uri);
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "GET";
            request.ContentType = "application/xml";


            // get response
            WebResponse response = request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    string responseFromServer = reader.ReadToEnd();
                    this.LoadItemFromXml(responseFromServer, true);
                }
            }
            response.Close();
        }


        /// <summary>
        /// Return a list of people involved in this project.
        /// </summary>
        /// <returns></returns>
        public List<Involvement> GetInvolvements()
        {
            Involvement obj = new Involvement(this.Settings);
            return obj.GetListByProjectId(this.Id);
        }

        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// No options
        /// </summary>
        public List<Project> GetList()
        {
            return this.GetListHelper(this.GetUriGetList(), null).ConvertAll<Project>(x => (Project)x);
        }
        #endregion


        #region Non-public methods
        #endregion
    }


    public class ProjectActivityOptions : BaseOptions
    {
        [IncludeInGetRequest]
        [XmlName("end-date")]
        public DateTime EndDate;

        [IncludeInGetRequest]
        [XmlName("limit")]
        public int Limit;

        [IncludeInGetRequest]
        [XmlName("start-date")]
        public DateTime StartDate;
    }
    public class ProjectSearchOptions : BaseOptions
    {
        [IncludeInGetRequest]
        [XmlName("end-index")]
        public int EndIndex;

        [IncludeInGetRequest]
        [XmlName("limit")]
        public int Limit;

        [IncludeInGetRequest]
        [XmlName("query")]
        public string Query;

        [IncludeInGetRequest]
        [XmlName("start-index")]
        public int StartIndex;

        [IncludeInGetRequest]
        [XmlName("filter")]
        public SearchFilter Filter;
    }
}
