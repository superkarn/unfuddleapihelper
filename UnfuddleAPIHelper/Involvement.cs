﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#involvement
    ///
    ///<involvement>
    ///    <created-at type="datetime"> </created-at>
    ///    <id type="integer"> </id>
    ///    <!--
    ///    Within the Involvement model, setting 'is-administrator' to true will
    ///    make the person a project administrator for the given project.
    ///    -->
    ///    <is-administrator type="boolean"> [true, false] </is-administrator>
    ///    <messages> [none, read, readcreate, manage] </messages>
    ///    <milestones> [none, read, manage] </milestones>
    ///    <notebooks> [none, read, manage] </notebooks>
    ///    <people> [none, read, invite, manage] </people>
    ///    <person-id type="integer"> </person-id>
    ///    <project-id type="integer"> </project-id>
    ///    <source> [none, read, commit] </source>
    ///    <tickets> [none, read, create, readcreate, manage] </tickets>
    ///    <updated-at type="datetime"> </updated-at>
    ///</involvement>
    [XmlName("involvement")]
    [XmlListName("involvements")]
    public class Involvement : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [XmlName("is-administrator")]
        public int IsAdministrator;

        [IncludeInXml]
        [XmlName("messages")]
        public PermissionMessage Messages;

        [IncludeInXml]
        [XmlName("milestones")]
        public PermissionMilestone Milestones;

        [IncludeInXml]
        [XmlName("notebooks")]
        public PermissionNotebook Notebooks;

        [IncludeInXml]
        [XmlName("people")]
        public PermissionPeople People;

        [IncludeInXml]
        [XmlName("person-id")]
        public int PersonId;

        [IncludeInXml]
        [XmlName("project-id")]
        public int ProjectId;

        [IncludeInXml]
        [XmlName("source")]
        public PermissionSource Source;

        [IncludeInXml]
        [XmlName("tickets")]
        public PermissionTicket Tickets;


        [IncludeInXml]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion


        public Involvement(Settings settings)
            : base(settings)
        { }


        /// <summary>
        /// Return a list of project the person is involved in.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Involvement> GetListByPersonId(int id)
        {
            this.CheckRequiredData();
            if (id == 0) throw new Exception("Person Id is empty.");

            string uri =
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/people/{3}/involvements",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    id
                );

            return this.GetListHelper(uri, null).ConvertAll<Involvement>(x => (Involvement)x);
        }
        /// <summary>
        /// Return a list of people involved with this project
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Involvement> GetListByProjectId(int id)
        {
            this.CheckRequiredData();
            if (id == 0) throw new Exception("Project Id is empty.");

            string uri =
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/involvements",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    id
                );

            return this.GetListHelper(uri, null).ConvertAll<Involvement>(x => (Involvement)x);
        }
    }
}
