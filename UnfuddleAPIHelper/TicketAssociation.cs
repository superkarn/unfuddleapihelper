﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/tickets
    /// http://unfuddle.com/docs/api/data_models#ticket_association
    /// 
    ///<ticket-association>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <relationship> [parent, duplicate, related] </relationship>
    ///  <ticket1-id type="integer"> </ticket1-id>
    ///  <ticket2-id type="integer"> </ticket2-id>
    ///</ticket-association>
    /// </summary>
    [XmlName("ticket-association")]
    [XmlListName("ticket-associations")]
    public class TicketAssociation : BaseModel
    {
        public int ProjectId;
        public int TicketId { get { return this.Ticket1Id; } set { this.Ticket1Id = value; } }


        #region XML Fields
        [IncludeInXml]
        [XmlName("relationship")]
        public Relationship Relationship;

        [IncludeInXml]
        [RequiredInXml]
        [XmlName("ticket1-id")]
        public int Ticket1Id;

        [IncludeInXml]
        [RequiredInXml]
        [XmlName("ticket2-id")]
        public int Ticket2Id;
        #endregion



        public TicketAssociation(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/ticket_associations",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/ticket_associations/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/ticket_associations/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/ticket_associations",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/ticket_associations/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<TicketAssociation> GetList()
        {
            return this.GetListHelper(this.GetUriGetList(), null).ConvertAll<TicketAssociation>(x => (TicketAssociation)x);
        }
        #endregion




        #region Non-public methods
        protected override void CheckRequiredData()
        {
            base.CheckRequiredData();


            if (this.ProjectId == 0) throw new Exception("ProjectId is empty.");
            if (this.TicketId == 0) throw new Exception("TicketId is empty.");
        }
        #endregion
    }
}
