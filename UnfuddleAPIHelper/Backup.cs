﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#backup
    ///
    ///<backup>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <parts type"integer"> </parts>
    ///  <processed type="boolean"> [true, false] </processed>
    ///  <processed-at type="datetime"> </processed-at>
    ///  <project-id type="integer"> </project-id>
    ///  <requester-id type="integer"> </requester-id>
    ///</backup>
    /// </summary>
    [XmlName("backup")]
    public class Backup : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [ReadOnly]
        [XmlName("parts")]
        public int Parts;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("processed")]
        public bool Proccessed;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("processed-at")]
        public DateTime ProccessedAt;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("project-id")]
        public int ProjectId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("requester-id")]
        public int RequesterId;
        #endregion


        public Backup(Settings settings)
            : base(settings)
        { }
    }
}
