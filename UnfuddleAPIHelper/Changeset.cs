﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#changeset
    /// 
    ///<changeset>
    ///  <!--
    ///  Note that not all repositories systems distinguish between
    ///  the author and committer (i.e. Subversion does not)
    ///  -->
    ///  <author-id type="integer"> </author-id>
    ///  <author-name> </author-name>
    ///  <author-email> </author-email>
    ///  <author-date type="datetime"> <!-- the date the patch was authored --> </author-date>
    ///  <committer-id type="integer"> </committer-id>
    ///  <committer-name> </committer-name>
    ///  <committer-email> </committer-email>
    ///  <committer-date type="datetime"> <!-- the date of the commit --> </committer-date>
    ///  <created-at type="datetime"> </created-at>
    ///  <id type="integer"> </id>
    ///  <message> </message>
    ///  <message-formatted> <!-- only available if formatted=true --> </message-formatted>
    ///  <repository-id type="integer"> </repository-id>
    ///  <revision> </revision>
    ///</changeset>
    /// </summary>
    [XmlName("changeset")]
    [XmlListName("changesets")]
    public class Changeset : BaseModel
    {
        public int ProjectId;
        public int TicketId;


        #region XML Fields
        [IncludeInXml]
        [XmlName("author-id")]
        public int AuthorId;

        [IncludeInXml]
        [XmlName("author-name")]
        public string AuthorName;

        [IncludeInXml]
        [XmlName("author-email")]
        public string AuthorEmail;

        [IncludeInXml]
        [XmlName("author-date")]
        public DateTime AuthorDate;

        [IncludeInXml]
        [XmlName("committer-id")]
        public int CommitterId;

        [IncludeInXml]
        [XmlName("committer-name")]
        public string CommitterName;

        [IncludeInXml]
        [XmlName("committer-email")]
        public string CommitterEmail;

        [IncludeInXml]
        [XmlName("committer-date")]
        public DateTime CommitterDate;

        [IncludeInXml]
        [XmlName("message")]
        public string Message;

        //[IncludeInXml]
        //[ReadOnly]
        //[XmlName("message-formatted")]
        //public bool MessageFormatted;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("repository-id")]
        public int RepositoryId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("revision")]
        public string Revision;
        #endregion


        public Changeset(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            throw new NotImplementedException();
        }
        protected override string GetUriDelete()
        {
            throw new NotImplementedException();
        }
        protected override string GetUriGetById(int id)
        {
            throw new NotImplementedException();
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/associated_changesets",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriUpdate()
        {
            throw new NotImplementedException();
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// No options
        /// </summary>
        public List<Changeset> GetList()
        {
            return this.GetListHelper(this.GetUriGetList(), null).ConvertAll<Changeset>(x => (Changeset)x);
        }
        #endregion
    }
}
