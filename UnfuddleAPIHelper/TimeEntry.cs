﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/data_models#time_entry
    /// 
    ///<time-entry>
    ///  <created-at type="datetime">  </created-at>
    ///  <date type="date"> </date>
    ///  <description> </description>
    ///  <hours type="float"> </hours>
    ///  <id type="integer"> </id>
    ///  <person-id type="integer"> </person-id>
    ///  <ticket-id type="integer"> </ticket-id>
    ///  <updated-at type="datetime"> </updated-at>
    ///</time-entry>
    /// </summary>
    [XmlName("time-entry")]
    [XmlListName("time-entries")]
    public class TimeEntry : BaseModel
    {
        public int ProjectId;


        #region XML Fields
        [IncludeInXml]
        [ReadOnly]
        [XmlName("date")]
        public DateTime Date;

        [IncludeInXml]
        [XmlName("description")]
        public string Description;

        [IncludeInXml]
        [XmlName("hours")]
        public float Hours;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("person-id")]
        public int PersonId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("ticket-id")]
        public int TicketId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;
        #endregion


        public TimeEntry(Settings settings)
            : base(settings)
        { }




        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/time_entries",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/time_entries/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/time_entries/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/time_entries",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}/time_entries/{5}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.TicketId,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<TimeEntry> GetList()
        {
            return this.GetListHelper(this.GetUriGetList(), null).ConvertAll<TimeEntry>(x => (TimeEntry)x);
        }
        #endregion




        #region Non-public methods
        protected override void CheckRequiredData()
        {
            base.CheckRequiredData();


            if (this.ProjectId == 0) throw new Exception("ProjectId is empty.");
            if (this.TicketId == 0) throw new Exception("TicketId is empty.");
        }
        #endregion
    }
}
