﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace UnfuddleAPIHelper
{
    /// <summary>
    /// http://unfuddle.com/docs/api/tickets
    /// http://unfuddle.com/docs/api/data_models#ticket
    /// 
    ///<ticket>
    ///  <assignee-id type="integer"> </assignee-id>
    ///  <component-id type="integer"> </component-id>
    ///  <created-at type="datetime"> </created-at>
    ///  <description> </description>
    ///  <description-format> [markdown, textile, plain] </description-format>
    ///  <description-formatted> <!-- only available if formatted=true --> </description-formatted>
    ///  <due-on type="date"> </due-on>
    ///  <due-on-formatted> </due-on-formatted>
    ///  <field1-value-id="integer"> </field1-value-id>
    ///  <field2-value-id="integer"> </field2-value-id>
    ///  <field3-value-id="integer"> </field3-value-id>
    ///  <hours-estimate-current type="float"> </hours-estimate-current>
    ///  <hours-estimate-initial type="float"> </hours-estimate-initial>
    ///  <id type="integer"> </id>
    ///  <milestone-id type="integer"> </milestone-id>
    ///  <number type="integer"> </number>
    ///  <priority> [1, 2, 3, 4, 5] </priority>
    ///  <project-id type="integer"> </project-id>
    ///  <reporter-id type="integer"> </reporter-id>
    ///  <resolution> [fixed, works_for_me, postponed, duplicate, will_not_fix, invalid] </resolution>
    ///  <resolution-description> </resolution-description>
    ///  <resolution-description-format> [markdown, textile, plain] </resolution-description-format>
    ///  <resolution-description-formatted> <!-- only available if formatted=true --> </resolution-description-formatted>
    ///  <severity-id type="integer"> </severity-id>
    ///  <status> [new, unaccepted, reassigned, reopened, accepted, resolved, closed] </status>
    ///  <summary> </summary>
    ///  <updated-at type="datetime"> </updated-at>
    ///  <version-id type="integer"> </version-id>
    ///    
    ///  <!--
    ///  The following are not actual ticket attributes, but when creating or updating
    ///  a ticket, including any of these three attributes instead of the corresponding
    ///  <fieldN-value-id> attributes will allow you to create a new value for
    ///  the ticekt field if that field is of the "text" disposition.
    ///  -->
    ///  <field1-value> </field1-value>
    ///  <field2-value> </field2-value>
    ///  <field3-value> </field3-value>
    ///  
    ///  <comments type="array"><!-- only available for GET requests if comments=true -->
    ///    <comment></comment>
    ///    ...
    ///  </comments>
    ///  
    ///  <attachments type="array"><!-- only available for GET requests if attachments=true -->
    ///    <attachment>...</attachment>
    ///    ...
    ///  </attachments>
    ///</ticket>
    /// </summary>
    [XmlName("ticket")]
    [XmlListName("tickets")]
    public class Ticket : BaseModel
    {
        #region XML Fields
        [IncludeInXml]
        [XmlName("assignee-id")]
        public int AssigneeId;

        [IncludeInXml]
        [XmlName("component-id")]
        public int ComponentId;

        [IncludeInXml]
        [XmlName("description")]
        public string Description;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("description-format")]
        public MarkupFormat DescriptionFormat;

        // Not implemented yet
        //[IncludeInXml]
        //[ReadOnly]
        //[XmlName("description-formatted")]
        //public bool DescriptionFormatted;

        [IncludeInXml]
        [XmlName("due-on")]
        public DateTime DueOn;

        // Not implemented yet
        //[IncludeInXml]
        //[ReadOnly]
        //[XmlName("due-on-formatted")]
        //public bool DueOnFormatted;

        [IncludeInXml]
        [XmlName("hours-estimate-current")]
        public float HoursEstimateCurrent;

        [IncludeInXml]
        [XmlName("hours-estimate-initial")]
        public float HoursEstimateInitial;

        [IncludeInXml]
        [XmlName("milestone-id")]
        public int MilestoneId;

        [IncludeInXml]
        [XmlName("number")]
        public int Number;

        [IncludeInXml]
        [RequiredInXml]
        [XmlName("priority")]
        public Priority Priority;

        [IncludeInXml]
        [ReadOnly]
        [RequiredInXml]
        [XmlName("project-id")]
        public int ProjectId;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("reporter-id")]
        public int ReporterId;

        [IncludeInXml]
        [XmlName("resolution")]
        public Resolution Resolution;

        [IncludeInXml]
        [XmlName("resolution-description")]
        public string ResolutionDescription;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("resolution-description-format")]
        public MarkupFormat ResolutionDescriptionFormat;

        // Not implemented yet
        //[IncludeInXml]
        //[ReadOnly]
        //[XmlName("resolution-description-formatted")]
        //public bool ResolutionDescriptionFormatted;

        [IncludeInXml]
        [XmlName("severity-id")]
        public int SeverityId;

        [IncludeInXml]
        [RequiredInXml]
        [XmlName("status")]
        public Status Status;

        [IncludeInXml]
        [RequiredInXml]
        [XmlName("summary")]
        public string Summary;

        [IncludeInXml]
        [ReadOnly]
        [XmlName("updated-at")]
        public DateTime UpdatedAt;

        [IncludeInXml]
        [XmlName("version-id")]
        public int VersionId;
        #endregion



        public Ticket(Settings settings)
            : base(settings)
        { }


        #region Get Uri methods
        protected override string GetUriCreate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId
                );
        }
        protected override string GetUriDelete()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.Id
                );
        }
        protected override string GetUriGetById(int id)
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    id
                );
        }
        protected override string GetUriGetList()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId
                );
        }
        protected override string GetUriUpdate()
        {
            return
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/{4}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    this.Id
                );
        }
        #endregion


        #region Public methods
        /// <summary>
        /// Returns a list of associated changesets.
        /// No options.
        /// </summary>
        public List<Changeset> GetAssociatedChangesets()
        {
            Changeset obj = new Changeset(this.Settings);
            obj.ProjectId = this.ProjectId;
            obj.TicketId = this.Id;
            return obj.GetList();
        }

        /// <summary>
        /// Given the ticket number, call Unfuddle and load the data into the current object.
        /// </summary>
        public void GetByNumber(int number)
        {
            this.CheckRequiredData();
            if (number == 0) throw new Exception("Number  is required.");

            string uri =
                string.Format("{0}://{1}.unfuddle.com/api/v{2}/projects/{3}/tickets/by_number/{4}",
                    (this.Settings.UseSsl ? "https" : "http"),
                    this.Settings.Subdomain,
                    this.Settings.ApiVersion,
                    this.ProjectId,
                    number
                );


            // send request
            WebRequest request = WebRequest.Create(uri);
            request.Credentials = new NetworkCredential(this.Settings.Username, this.Settings.Password);
            request.Method = "GET";
            request.ContentType = "application/xml";


            // get response
            WebResponse response = request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    string responseFromServer = reader.ReadToEnd();
                    this.LoadItemFromXml(responseFromServer, true);
                }
            }
            response.Close();
        }

        /// <summary>
        /// Return a list of this object.
        /// Calls the GetListHelper() then conert it to the current object's type
        /// </summary>
        public List<Ticket> GetList(TicketListOptions options = null)
        {
            return this.GetListHelper(this.GetUriGetList(), options).ConvertAll<Ticket>(x => (Ticket)x);
        }

        /// <summary>
        /// Returns a list of associated TicketAssociation
        /// No options
        /// </summary>
        public List<TicketAssociation> GetTicketAssociations()
        {
            TicketAssociation obj = new TicketAssociation(this.Settings);
            obj.ProjectId = this.ProjectId;
            obj.TicketId = this.Id;
            return obj.GetList();
        }

        /// <summary>
        /// Returns a list of Time Entries
        /// No options
        /// </summary>
        public List<TimeEntry> GetTimeEntries()
        {
            TimeEntry obj = new TimeEntry(this.Settings);
            obj.ProjectId = this.ProjectId;
            obj.TicketId = this.Id;
            return obj.GetList();
        }
        #endregion




        #region Non-public methods
        protected override void CheckRequiredData()
        {
            base.CheckRequiredData();


            if (this.ProjectId == 0) throw new Exception("ProjectId is empty.");
        }
        #endregion
    }


    public class TicketListOptions : BaseOptions
    {
        [IncludeInGetRequest]
        [XmlName("truncate")]
        public int Truncate;
    }
    public class TicketReportOptions : BaseOptions
    {
        // __TODO__ update the rest of the options

        //[IncludeInGetRequest]
        //[XmlName("conditions-string")]
        //public object ConditionsString;

        //[IncludeInGetRequest]
        //[XmlName("fields-string")]
        //public object FieldsString;

        [IncludeInGetRequest]
        [XmlName("group-by")]
        public SearchGroupBy GroupBy;

        [IncludeInGetRequest]
        [XmlName("sort-by")]
        public SearchSortBy SortBy;

        [IncludeInGetRequest]
        [XmlName("sort-direction")]
        public SearchSortDirection SortDirection;

        [IncludeInGetRequest]
        [XmlName("title")]
        public string Title;
    }
}
